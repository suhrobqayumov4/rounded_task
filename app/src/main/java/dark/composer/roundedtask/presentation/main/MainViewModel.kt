package dark.composer.roundedtask.presentation.main

import android.app.Activity
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dark.composer.roundedtask.R
import dark.composer.roundedtask.data.base.BaseNetworkResult
import dark.composer.roundedtask.data.local.Model
import dark.composer.roundedtask.data.local.room.entities.ExerciseEntity
import dark.composer.roundedtask.data.remote.models.ImageResponse
import dark.composer.roundedtask.domain.use_case.MainUseCase
import dark.composer.roundedtask.utils.Constants
import dark.composer.roundedtask.utils.getFullName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val useCase: MainUseCase) : ViewModel() {

    private val _list = MutableLiveData<ArrayList<ExerciseEntity>>()
    val list: LiveData<ArrayList<ExerciseEntity>> = _list

    private val _id = MutableLiveData<Long>()
    val id: LiveData<Long> = _id

    private val _path = MutableLiveData<String>()
    val path: LiveData<String> = _path

    private val _updateListener = MutableLiveData<String>()
    val updateListener: LiveData<String> = _updateListener

    private val _image = MutableSharedFlow<BaseNetworkResult<ImageResponse>>()
    val image = _image.asSharedFlow()

    fun getImage() {
        viewModelScope.launch {
            useCase.getRandomImages().onEach { result ->
                when (result) {
                    is BaseNetworkResult.Error -> {
                        _image.emit(BaseNetworkResult.Error(result.message))
                    }
                    is BaseNetworkResult.Loading -> {
                        _image.emit(BaseNetworkResult.Loading(result.isLoading))
                    }
                    is BaseNetworkResult.Success -> {
                        result.data?.let {
                            _image.emit(BaseNetworkResult.Success(it))
                        }
                    }
                }
            }.launchIn(viewModelScope)
        }
    }

    init {
        getExercises()
    }

    private fun getExercises() {
        useCase.getExercise().onEach {
            if (it.isEmpty()) {
                setExercise()
            } else {
                _list.postValue(it)
                Log.d("FFFFFF", "getExercises:$it")
            }
        }.launchIn(CoroutineScope(Dispatchers.IO))
    }

    fun update(id: Int, isCheck: Int) {
        useCase.updateExercise(id = id, isCheck = isCheck).onEach {
            Log.d("NNNNN", "updateViewModel: keldi $isCheck, $id")
            _updateListener.postValue(it)
        }.launchIn(CoroutineScope(Dispatchers.IO))
    }

    private fun setExercise() {
        viewModelScope.launch(Dispatchers.IO) {
            val models = arrayListOf<ExerciseEntity>()
            models.add(
                ExerciseEntity(
                    name = "Grammar",
                    image = R.drawable.group,
                    color = R.color.grammar,
                    id = 0
                )
            )
            models.add(
                ExerciseEntity(
                    name = "Vocabulary",
                    image = R.drawable.book,
                    color = R.color.vocabulary,
                    id = 1
                )
            )
            models.add(
                ExerciseEntity(
                    name = "Speaking",
                    image = R.drawable.speaking,
                    color = R.color.speaking,
                    id = 2
                )
            )
            models.add(
                ExerciseEntity(
                    name = "Listening",
                    image = R.drawable.headphones,
                    color = R.color.listening,
                    id = 3,
                )
            )
            models.add(
                ExerciseEntity(
                    name = "Homework",
                    image = R.drawable.trophy,
                    color = R.color.homework,
                    subTitle = "Bu joyda barcha ishtirokchilar darajalari bilan tanishing",
                    id = 4
                )
            )
            _list.postValue(models)
            useCase.addExercise(models)
        }
    }

    fun download(url: String, context: Activity, br: BroadcastReceiver) {
        try {
            val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val videoUrl = Uri.parse(url)
            val request = DownloadManager.Request(videoUrl)
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                .setDescription("Downloading...").setAllowedOverMetered(true)
                .setVisibleInDownloadsUi(false)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setTitle("Download")
            request.allowScanningByMediaScanner()

            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS, "/lights/${getFullName(Constants.VIDEO_URL)}"
            )
            val myId = manager.enqueue(request)
            _id.value = myId
            _path.value = "${Constants.BASE_EXTERNAL_STORAGE_URL}${getFullName(Constants.VIDEO_URL)}"
            context.registerReceiver(br, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}