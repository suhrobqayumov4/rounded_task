package dark.composer.roundedtask.presentation.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import dark.composer.roundedtask.data.base.BaseNetworkResult
import dark.composer.roundedtask.data.local.room.entities.ExerciseEntity
import dark.composer.roundedtask.data.manager.CheckNetworkConnection
import dark.composer.roundedtask.databinding.FragmentMainBinding
import dark.composer.roundedtask.presentation.BaseFragment
import dark.composer.roundedtask.presentation.dialog.ImageDialog
import dark.composer.roundedtask.presentation.main.adapter.MainAdapter
import dark.composer.roundedtask.utils.*
import kotlinx.coroutines.launch
import java.io.File

@AndroidEntryPoint
class MainFragment : BaseFragment<FragmentMainBinding>(FragmentMainBinding::inflate) {
    companion object {
        const val PERMISSION_REQUEST_CODE = 1
    }

    private val viewModel: MainViewModel by viewModels()

    private val adapter by lazy {
        MainAdapter()
    }

    private val sharedPref by lazy {
        SharedPref(requireContext())
    }

    private val dialog by lazy {
        ImageDialog(requireContext())
    }

    private lateinit var checkNetworkConnection: CheckNetworkConnection

    private val controller by lazy {
        MediaController(requireContext())
    }

    private val br = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            viewModel.id.observe(viewLifecycleOwner) { it ->
                if (id == it) {
                    binding.progressDownload.visibility = View.GONE
                    binding.lin8.visibility = View.VISIBLE
                    viewModel.path.observe(viewLifecycleOwner) { t ->
                        setUpVideo(t)
                    }
                    Toast.makeText(
                        requireContext(),
                        "Success downloaded ${getName(Constants.VIDEO_URL)}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    override fun onViewCreate() {
        setUi()
        observe()
    }

    private fun setUi() {
        binding.list.adapter = adapter

        dialogShow()

        setUpHomeWork()

        adapter.setItemClickListener { id, isCheck ->
            Log.d("NNNN", "setUi: click")
            viewModel.update(id, isCheck)
        }

        binding.play.setOnClickListener {
            checkPermission(2)
        }

        binding.download.setOnClickListener {
            checkPermission(1)
        }

        dialog.setOnStopListener {
            sharedPref.setStop(true)
        }
    }

    private fun observe() {
        viewModel.list.observe(this) {
            val r = ArrayList<ExerciseEntity>()
            for (i in 0 until 4)
                r.add(it[i])
            if (it[4].isCheck == 1) {
                binding.line7.visibility = View.VISIBLE
            } else {
                binding.line7.visibility = View.GONE
            }
            binding.text.text = it[4].name
            binding.text1.text = it[4].subTitle
            binding.imageView2.setImageResource(it[4].image)
            binding.card.setCardBackgroundColor(it[4].color)
            adapter.set(r)
        }

        viewModel.updateListener.observeForever {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.whenStarted {
                viewModel.image.collect {
                    when (it) {
                        is BaseNetworkResult.Success -> {
                            it.data?.let { detail ->
                                dialog.setImage(detail.urls.small)
                            }
                        }
                        is BaseNetworkResult.Error -> {
                            Toast.makeText(
                                requireContext(), it.message.toString(), Toast.LENGTH_SHORT
                            ).show()
                        }
                        is BaseNetworkResult.Loading -> {
                            dialog.setProgress()
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setUpHomeWork() {
        var i = 0
        binding.card.setOnClickListener {
            i++
            val handler = Handler()
            val runnable = Runnable {
                kotlin.run {
                    i = 0
                }
            }
            when (i) {
                1 -> {
                    handler.postDelayed(runnable, 500)
                }
                3 -> {
                    if (!binding.line7.isVisible) {
                        viewModel.update(4, 1)
                        binding.line7.visibility = View.VISIBLE
                    }
                }
            }
        }

        var then: Long = 0;
        val duration = 5000
        if (binding.line7.isVisible) {
            binding.line7.setOnTouchListener(View.OnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_DOWN) {
                    then = System.currentTimeMillis() as Long
                } else if (event.action == MotionEvent.ACTION_UP) {
                    if (System.currentTimeMillis() - then > duration) {
                        /* Long click behaviour will go here*/
                        viewModel.update(4, 0)
                        binding.line7.visibility = View.GONE
                        return@OnTouchListener false
                    } else {
                        return@OnTouchListener false
                    }
                }
                true
            })
        }
    }

    private fun searchFile(path: String) {
        val file = File("${Constants.BASE_EXTERNAL_STORAGE_URL}$path")
        Log.d("BBBBB", "setUpVideo: /storage/emulated/0/Download/lights/$path")
        if (!file.exists()) {
            viewModel.download(Constants.VIDEO_URL, requireActivity(), br)
            videoExtensions()
        } else {
            setUpVideo("${Constants.BASE_EXTERNAL_STORAGE_URL}$path")
            Toast.makeText(requireContext(), "This file already exists", Toast.LENGTH_SHORT).show()
        }
    }

    private fun videoExtensions() {
        binding.progressDownload.visibility = View.VISIBLE
        binding.lin8.visibility = View.GONE
        binding.video.visibility = View.GONE
        binding.card1.visibility = View.VISIBLE
    }

    private fun setUpVideo(path: String) {
        binding.video.visibility = View.VISIBLE
        binding.card1.visibility = View.GONE
        binding.video.setVideoPath(path)
        binding.video.setMediaController(controller)
        controller.setAnchorView(binding.video)
    }

    private fun checkPermission(int: Int) {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                PERMISSION_REQUEST_CODE
            )
        } else {
            if (int == 1) {
                searchFile(getFullName(Constants.VIDEO_URL))
            } else {
                setUpVideo(Constants.VIDEO_URL)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(requireContext(), "Storage Permission Granted", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(requireContext(), "Storage Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun dialogShow() {
        checkNetworkConnection = CheckNetworkConnection(requireActivity().application)
        checkNetworkConnection.observe(this) { isConnected ->
            if (isConnected) {
                if (!sharedPref.getStop()) {
                    dialog.show()
                    viewModel.getImage()
                }
            } else {
                Snackbar.make(binding.pic1, "Internet off", Snackbar.LENGTH_LONG).show()
            }
        }
    }

    override fun onStop() {
        dialog.dismiss()
        super.onStop()
    }

    override fun onPause() {
        dialog.dismiss()
        activity?.unregisterReceiver(br)
        super.onPause()
    }

    override fun onResume() {
        dialogShow()
        activity?.registerReceiver(
            br, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
        super.onResume()
    }

    override fun onStart() {
        dialogShow()
        super.onStart()
    }
}
