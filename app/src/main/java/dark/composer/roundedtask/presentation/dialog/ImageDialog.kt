package dark.composer.roundedtask.presentation.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.lifecycle.VIEW_MODEL_STORE_OWNER_KEY
import com.bumptech.glide.Glide
import dark.composer.roundedtask.databinding.DialogBinding

class ImageDialog(content: Context) : AlertDialog(content) {
    var binding: DialogBinding = DialogBinding.inflate(layoutInflater)

    private var stopListener: ((amount: Int) -> Unit)? = null

    fun setOnStopListener(f: (amount: Int) -> Unit) {
        stopListener = f
    }

    fun setImage(url: String) {
        binding.progress.visibility = View.GONE
        binding.image.visibility = View.VISIBLE
        Glide.with(binding.root).load(url).into(binding.image)
    }

    fun setProgress() {
        binding.progress.visibility = View.VISIBLE
        binding.image.visibility = View.GONE
    }

    init {
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        binding.exit.setOnClickListener {
            dismiss()
        }

        binding.stop.setOnClickListener {
            stopListener?.invoke(1)
            dismiss()
        }

        setView(binding.root)
    }
}