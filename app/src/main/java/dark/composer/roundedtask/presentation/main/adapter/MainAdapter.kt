package dark.composer.roundedtask.presentation.main.adapter

import android.os.Handler
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import dark.composer.roundedtask.data.local.Model
import dark.composer.roundedtask.data.local.room.entities.ExerciseEntity
import dark.composer.roundedtask.databinding.ItemPlayBinding

class   MainAdapter : RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    private val list = arrayListOf<ExerciseEntity>()

    private var itemClickListener: ((id: Int, isCheck: Int) -> Unit)? = null

    fun setItemClickListener(f: (id: Int, isCheck: Int) -> Unit) {
        itemClickListener = f
    }

    fun set(list: ArrayList<ExerciseEntity>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemPlayBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ExerciseEntity) {
            binding.itemType.setImageResource(data.image)
            binding.vidTV.text = data.name
            binding.theme.setBackgroundResource(data.color)

            if (data.isCheck == 0) {
                binding.line1.visibility = View.GONE
            } else {
                binding.line1.visibility = View.VISIBLE
            }

            var i = 0
            binding.card.setOnClickListener {
                i++
                val handler = Handler()
                val runnable = Runnable {
                    kotlin.run {
                        i = 0
                    }
                }
                when (i) {
                    1 -> {
                        handler.postDelayed(runnable, 500)
                    }
                    3 -> {
                        if (!binding.line1.isVisible) {
                            itemClickListener?.invoke(data.id, 1)
                            binding.line1.visibility = View.VISIBLE
                        }
                    }
                }
            }

            var then: Long = 0;
            val duration = 5000
            if (binding.line1.isVisible) {
                binding.line1.setOnTouchListener(View.OnTouchListener { v, event ->
                    if (event.action == MotionEvent.ACTION_DOWN) {
                        then = System.currentTimeMillis() as Long
                    } else if (event.action == MotionEvent.ACTION_UP) {
                        if (System.currentTimeMillis() - then >= duration) {
                            /* Long click behaviour will go here*/
                            itemClickListener?.invoke(data.id, 0)
                            binding.line1.visibility = View.GONE
                            return@OnTouchListener false
                        } else {
                            /* LONG CLICK FAILED*/
                            return@OnTouchListener false
                        }
                    }
                    true
                })
            }

//            if (binding.line1.longPress()) {
//                Toast.makeText(binding.root.context, "Long", Toast.LENGTH_SHORT).show()
//            }
//            if (binding.card.click(binding.line1)) {
//                Toast.makeText(binding.root.context, "Triple", Toast.LENGTH_SHORT).show()
////                itemClickListener?.invoke(data.id,0)
//            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemPlayBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(list[position])

    override fun getItemCount() = list.size
}