package dark.composer.roundedtask.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

class SharedPref(context: Context) {

    private var preferences: SharedPreferences =
        context.getSharedPreferences("APP_PREFS_NAME", MODE_PRIVATE)

    private lateinit var editor: SharedPreferences.Editor

    fun setStop(boolean: Boolean) {
        editor = preferences.edit()
        editor.putBoolean("STOP", boolean)
        editor.apply()
    }

    fun setVideo(path:String) {
        editor = preferences.edit()
        editor.putString("VIDEO", path)
        editor.apply()
    }

    fun getStop() = preferences.getBoolean("STOP", false)
    fun getVideo() = preferences.getString("VIDEO", "")
}
