package dark.composer.roundedtask.utils

import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Toast
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.commons.io.FilenameUtils
import retrofit2.http.Url
import java.io.File
import java.net.URL

fun getName(path: String): String {
    val url = Uri.parse(path)
    return FilenameUtils.getBaseName(url.path).lowercase()
}

fun getFullName(path:String): String {
    val url = Uri.parse(path)
    val r = FilenameUtils.getBaseName(url.path)
    val u = FilenameUtils.getExtension(url.path)
    getType(r)
    Log.d("YYYYYY", "getFullName: $r.${getType(u)}")
    return "$r.${getType(u)}".lowercase()
}

private fun getType(path: String): String {
    val url = Uri.parse(path)
    var r = FilenameUtils.getExtension(url.path)
    if (r != "mp4") {
        r = "mp4"
        return r
    }
    return r.lowercase()
}