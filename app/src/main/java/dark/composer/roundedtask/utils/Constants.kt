package dark.composer.roundedtask.utils

object Constants {
    const val BASE_URL = "https://api.unsplash.com"
    const val TOKEN = "Client-ID mT8hj53DywChJkbscZAN5aHio9v2M9impW_i-VIc7vs"
    const val IMAGE_URL = "/photos/random"
    const val BASE_EXTERNAL_STORAGE_URL = "/storage/emulated/0/Download/lights/"
    const val VIDEO_URL = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
    const val VIDEO_URL1 = "https://www.shutterstock.com/shutterstock/videos/1065170830/preview/stock-footage-nature-river-waterfall-forest-sun-morning-magical.webm"
}
