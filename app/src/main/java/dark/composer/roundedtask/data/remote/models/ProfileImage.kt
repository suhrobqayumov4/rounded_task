package dark.composer.roundedtask.data.remote.models

data class ProfileImage(
    val large: String,
    val medium: String,
    val small: String
)