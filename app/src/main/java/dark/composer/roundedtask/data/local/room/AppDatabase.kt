package dark.composer.roundedtask.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dark.composer.roundedtask.data.local.room.entities.ExerciseEntity

@Database(
    entities = [
        ExerciseEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getInputData(): InputDao

    companion object {
        var database: AppDatabase? = null
        fun init(context: Context):AppDatabase {
            database = Room.databaseBuilder(
                context.applicationContext, AppDatabase::class.java, "exercise_db"
            )
                .allowMainThreadQueries()
                .build()
            return database!!
        }
    }
}