package dark.composer.roundedtask.data.local.room

import androidx.lifecycle.LiveData
import androidx.room.*
import dark.composer.roundedtask.data.local.room.entities.ExerciseEntity


@Dao
interface InputDao {

    @Query("SELECT * FROM exercise")
    fun getAllTrainers(): List<ExerciseEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveTrainers(users: List<ExerciseEntity>)

    @Query("UPDATE exercise SET isCheck = :isCheck WHERE id = :id")
    fun updateUser(id: Int, isCheck: Int):Int

}