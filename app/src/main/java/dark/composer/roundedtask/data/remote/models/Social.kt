package dark.composer.roundedtask.data.remote.models

data class Social(
    val instagram_username: String,
    val paypal_email: Any,
    val portfolio_url: Any,
    val twitter_username: Any
)