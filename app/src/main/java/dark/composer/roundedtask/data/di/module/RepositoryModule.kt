package dark.composer.roundedtask.data.di.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dark.composer.roundedtask.data.local.room.InputDao
import dark.composer.roundedtask.data.remote.ApiService
import dark.composer.roundedtask.data.repo.MainRepositoryImpl
import dark.composer.roundedtask.domain.repo.MainRepository

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    fun provideServiceRepository(mainService: ApiService,inputDao: InputDao): MainRepository {
        return MainRepositoryImpl(mainService,inputDao)
    }
}