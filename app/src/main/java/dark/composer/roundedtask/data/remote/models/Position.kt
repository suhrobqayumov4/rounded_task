package dark.composer.roundedtask.data.remote.models

data class Position(
    val latitude: Double,
    val longitude: Double
)