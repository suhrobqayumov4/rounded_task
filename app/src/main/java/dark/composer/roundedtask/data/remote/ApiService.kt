package dark.composer.roundedtask.data.remote

import dark.composer.roundedtask.data.remote.models.ImageResponse
import dark.composer.roundedtask.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {

    @GET(Constants.IMAGE_URL)
    suspend fun getRandomImages(
        @Header("Authorization") token: String = Constants.TOKEN,
    ): Response<ImageResponse>

}