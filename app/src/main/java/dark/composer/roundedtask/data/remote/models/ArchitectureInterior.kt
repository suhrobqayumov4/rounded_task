package dark.composer.roundedtask.data.remote.models

data class ArchitectureInterior(
    val approved_on: String,
    val status: String
)