package dark.composer.roundedtask.data.remote.models

data class Topic(
    val id: String,
    val slug: String,
    val title: String,
    val visibility: String
)