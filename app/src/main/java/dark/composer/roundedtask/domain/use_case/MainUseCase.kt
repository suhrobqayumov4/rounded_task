package dark.composer.roundedtask.domain.use_case

import androidx.lifecycle.LiveData
import dark.composer.roundedtask.data.base.BaseNetworkResult
import dark.composer.roundedtask.data.local.room.entities.ExerciseEntity
import dark.composer.roundedtask.data.remote.models.ImageResponse
import dark.composer.roundedtask.domain.repo.MainRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MainUseCase @Inject constructor(
    private val repo: MainRepository
) {
    fun getRandomImages(): Flow<BaseNetworkResult<ImageResponse>> {
        return repo.getRandomImages()
    }

    fun getExercise():Flow<ArrayList<ExerciseEntity>>{
        return repo.getExercise()
    }

    fun updateExercise(id:Int, isCheck:Int):Flow<String>{
        return repo.updateExercises(id,isCheck)
    }

    fun addExercise(list: ArrayList<ExerciseEntity>){
        return repo.addExercises(list)
    }
}