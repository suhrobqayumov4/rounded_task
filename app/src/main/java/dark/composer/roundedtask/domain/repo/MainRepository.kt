package dark.composer.roundedtask.domain.repo

import androidx.lifecycle.LiveData
import dark.composer.roundedtask.data.base.BaseNetworkResult
import dark.composer.roundedtask.data.local.room.InputDao
import dark.composer.roundedtask.data.local.room.entities.ExerciseEntity
import dark.composer.roundedtask.data.remote.models.ImageResponse
import kotlinx.coroutines.flow.Flow

interface MainRepository {
    fun getRandomImages(): Flow<BaseNetworkResult<ImageResponse>>
    fun getExercise(): Flow<ArrayList<ExerciseEntity>>
    fun addExercises(list:ArrayList<ExerciseEntity>)
    fun updateExercises(id: Int, isCheck:Int):Flow<String>
}